<?php

namespace BitbucketDeployWordpressPlugin;

/**
 * Class BitbucketDeployWebHook
 */
class WebHook
{
    const MODE_WORDPRESS = 'wordpress';

    const LOG_LEVEL_INFO = 'INFO';
    const LOG_LEVEL_ERROR = 'ERROR';

    const EVENT_KEY_PUSH = 'repo:push';
    const EVENT_KEY_MERGED_PR = 'pullrequest:fulfilled';

    const EVENT_TYPE_PUSH = 1;
    const EVENT_TYPE_MERGED_PR = 2;

    private $target_branch = 'master';

    private $name_repo;
    private $dir_web_root;
    private $bitbucket_user;
    private $bitbucket_pass;

    private $payload = [];
    private $do_update = false;

    private $write_log = true;
    private $display_log = true;
    private $log_file;
    private $logs = [];

    private $event_type;
    private $mode = self::MODE_WORDPRESS;

    private $is_linux_os = false;
    private $stopped = false;
    private $success = false;

    /**
     * WebHook constructor.
     */
    public function __construct()
    {
        $this->setLogFile(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'webhook.log');
        $this->is_linux_os = (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN');
    }

    /**
     * Processes data received from BitBucket
     * @link https://confluence.atlassian.com/bitbucket/event-payloads-740262817.html
     * @param string $posted_data
     */
    public function processPayload($posted_data)
    {
        if ($this->isStopped())
            return;

        if (!$posted_data)
            $this->logError('No data posted.', true);

        $payload = json_decode($posted_data, true);
        if (json_last_error() !== JSON_ERROR_NONE)
            $this->logError('Error while parsing JSON payload.', true);

        if (empty($payload))
            $this->logError('Empty payload.', true);

        $this->payload = $payload;
        $this->checkRepoMatch();
        $this->defineEventType();
    }

    /**
     * Checks if repo in payload matches repo set in parameters
     */
    private function checkRepoMatch()
    {
        if ($this->isStopped())
            return;

        if (!$this->getNameRepo())
            $this->logError("No target repo defined. We can't just deploy everything. Stopping execution.", true);

        if (!isset($this->payload['repository']))
            $this->logError("Payload has no repository data.", true);

        if ($this->payload['repository']['full_name'] !== $this->getNameRepo())
            $this->logError(sprintf(
                "Repo name in payload (%s) doesn't match with the one defined in webhook (%s). Ignoring this payload.",
                $this->payload['repository']['full_name'],
                $this->getNameRepo()
            ), true);
    }

    /**
     * Sets internal event type basing on payload
     */
    private function defineEventType()
    {
        if ($this->isStopped())
            return;

        if (!isset($_SERVER['HTTP_X_EVENT_KEY'])) {
            $this->logError("No X-Event-Key in request headers. Can't determine Event type.", true);
            return;
        }

        switch ($_SERVER['HTTP_X_EVENT_KEY']) {
            case self::EVENT_KEY_PUSH:
                $this->setEventType(self::EVENT_TYPE_PUSH);
                break;
            case self::EVENT_KEY_MERGED_PR:
                $this->setEventType(self::EVENT_TYPE_MERGED_PR);
                break;
            default:
                $this->logError(sprintf("Unknown event type (X-Event-Key HTTP header) = %s.", $_SERVER['HTTP_X_EVENT_KEY']), true);
                break;
        }
    }

    /**
     * Main function to determine if current payload signals that update is needed
     */
    public function checkIfUpdateNeeded()
    {
        if ($this->isStopped())
            return;

        $update = $this->checkIfPush();

        if (!$update)
            $update = $this->checkIfMerge();

        $this->setDoUpdate($update);
    }

    /**
     * Checks if payload has "push" data for pre-defined branch
     * @return bool
     */
    private function checkIfPush()
    {
        if ($this->isStopped())
            return false;

        if ($this->getEventType() !== self::EVENT_TYPE_PUSH)
            return false;

        if (!isset($this->payload['push']))
            $this->logError("Event type is set to PUSH but there's no `push` data in payload.", true);

        $lastChange = $this->payload['push']['changes'][count($this->payload['push']['changes']) - 1]['new'];
        $branch = isset($lastChange['name']) && !empty($lastChange['name']) ? $lastChange['name'] : '';

        $this->log(sprintf("checkIfPush(): branch in last change = %s ; target branch to track = %s", $branch, $this->getTargetBranch()));

        return $branch === $this->getTargetBranch();
    }

    /**
     * Checks if payload has "merged PR" data for pre-defined branch
     * @return bool
     */
    private function checkIfMerge()
    {
        if ($this->isStopped())
            return false;

        if ($this->getEventType() !== self::EVENT_TYPE_MERGED_PR)
            return false;

        if (!isset($this->payload['pullrequest']))
            $this->logError("Event type is set to MERGED_PR but there's no PR data in payload.", true);

        if (!isset($this->payload['pullrequest']['state']) or $this->payload['pullrequest']['state'] !== 'MERGED') {
            $this->log("Event type is set to MERGED_PR but PR in payload doesn't have MERGED status.");
            return false;
        }

        $branch = $this->payload['pullrequest']['destination']['branch']['name'];
        $this->log(sprintf("checkIfMerge(): branch in PR = %s ; target branch to track = %s ", $branch, $this->getTargetBranch()));

        return $branch === $this->getTargetBranch();
    }

    /**
     * Main update process, depending on calling type (Wordpress by default)
     * @param bool $manual if set to true function can be used without prior checking routine
     */
    public function update($manual = false)
    {
        if ($manual) {
            $this->setStopped(false);
            $this->setDoUpdate(true);
        }

        if ($this->isStopped())
            return;

        if (!$this->isDoUpdate()) {
            $this->log('Update is not needed.');
            return;
        }

        if (!$this->getDirWebRoot())
            $this->logError("No web root dir defined.", true);

        switch ($this->getMode()) {
            /**
             * Downloads archived branch
             * Extracts into target directory (relative to Wordpress base dir)
             */
            case self::MODE_WORDPRESS:
                $url = sprintf(
                    'https://bitbucket.org/%s/get/%s.zip',
                    $this->getNameRepo(),
                    $this->getTargetBranch()
                );

                $this->log(sprintf(
                    "Downloading file from %s",
                    $url
                ));

                $curl = curl_init();
                curl_setopt_array($curl, [
                    CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
                    CURLOPT_USERPWD => sprintf(
                        '%s:%s',
                        $this->getBitbucketUser(),
                        $this->getBitbucketPass()),
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                ]);
                $result = curl_exec($curl);
                $info = curl_getinfo($curl);
                curl_close($curl);

                if ($info['http_code'] !== 200) {
                    $this->logError(sprintf(
                        "Downloading wasn't completed as expected. cURL debug data: %s",
                        print_r($info, true)
                    ), true);
                    return;
                }

                $tmp_file = tempnam(sys_get_temp_dir(), __CLASS__);
                if (!$tmp_file)
                    $this->logError("Can't create temporary file.", true);

                $this->log(sprintf(
                    "Temporary file created at %s",
                    $tmp_file
                ));

                file_put_contents($tmp_file, $result);
                $this->log(sprintf(
                    "File downloaded (%s bytes), saved to %s",
                    strlen($result),
                    $tmp_file));

                $zip = new \ZipArchive();
                $zip->open($tmp_file);
                if ($zip) {
                    $directory_name = $zip->getNameIndex(0);
                    $tmp_dir = sys_get_temp_dir() . DIRECTORY_SEPARATOR . uniqid();

                    $ok = $zip->extractTo($tmp_dir);
                    if (!$ok)
                        $this->logError(sprintf(
                            "Can't extract content to %s",
                            $tmp_dir
                        ), true);

                    $this->log(sprintf(
                        "Content extracted to temp folder at %s",
                        $tmp_dir));

                    $this->log(sprintf(
                        "Cleaning up target directory at %s (to avoid possible conflicts)",
                        $this->getDirWebRoot()
                    ));
                    self::delTree($this->getDirWebRoot());

                    $this->log(sprintf("Renaming temp folder to %s", $this->getDirWebRoot()));
                    $ok = rename(
                        $tmp_dir . DIRECTORY_SEPARATOR . $directory_name,
                        $this->getDirWebRoot());

                    if ($ok) {
                        $this->log("Update completed!");
                        $this->setSuccess(true);
                    }

                    $zip->close();
                    self::delTree($tmp_dir);
                }

                @unlink($tmp_file);
                break;
        }
    }

    /**
     * Properly removes directory tree (including links and normal files)
     * @param string $dir
     * @return bool
     */
    public static function delTree($dir)
    {
        $files = array_diff(scandir($dir), array('.', '..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file") && !is_link($dir))
                ? self::delTree("$dir/$file")
                : @unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    /**
     * @return string
     */
    public function getTargetBranch()
    {
        return $this->target_branch;
    }

    /**
     * @param string $target_branch
     */
    public function setTargetBranch($target_branch)
    {
        $this->log(sprintf("Target branch is set to '%s'.", $target_branch));
        $this->target_branch = $target_branch;
    }

    /**
     * @return mixed
     */
    public function getDirWebRoot()
    {
        return $this->dir_web_root;
    }

    /**
     * @param mixed $dir_web_root
     */
    public function setDirWebRoot($dir_web_root)
    {
        $this->log(sprintf("Web root folder path is set to '%s'.", $dir_web_root));
        $this->dir_web_root = $dir_web_root;
    }

    /**
     * @return bool
     */
    public function isDoUpdate()
    {
        return $this->do_update;
    }

    /**
     * @param bool $do_update
     */
    public function setDoUpdate($do_update)
    {
        $this->do_update = $do_update;
    }

    /**
     * @param string $message
     * @param string $level
     * @param bool $stop_execution
     */
    private function log($message, $level = self::LOG_LEVEL_INFO, $stop_execution = false)
    {
        $string = sprintf(
            '[%s][%s] %s',
            $level,
            date('d.m.Y H:i:s'),
            $message);

        $this->logs [] = $string;

        if ($this->isDisplayLog())
            echo $string . '<br/>';

        if ($this->isWriteLog()) {
            $ok = touch($this->getLogFile());
            if ($ok and is_writeable($this->getLogFile()))
                file_put_contents($this->getLogFile(), $string . PHP_EOL, FILE_APPEND);
        }

        if ($stop_execution)
            $this->setStopped(true);
    }

    /**
     * @param string $message
     * @param bool $stop_execution
     */
    private function logError($message, $stop_execution = false)
    {
        $this->log($message, self::LOG_LEVEL_ERROR, $stop_execution);
    }

    /**
     * @return mixed
     */
    public function getEventType()
    {
        return $this->event_type;
    }

    /**
     * @param mixed $event_type
     */
    public function setEventType($event_type)
    {
        $this->event_type = $event_type;
    }

    /**
     * @return bool
     */
    public function isWriteLog()
    {
        return $this->write_log;
    }

    /**
     * @param bool $write_log
     */
    public function setWriteLog($write_log)
    {
        $this->write_log = $write_log;
    }

    /**
     * @return bool
     */
    public function isDisplayLog()
    {
        return $this->display_log;
    }

    /**
     * @param bool $display_log
     */
    public function setDisplayLog($display_log)
    {
        $this->display_log = $display_log;
    }

    /**
     * @return mixed
     */
    public function getNameRepo()
    {
        return $this->name_repo;
    }

    /**
     * @param mixed $name_repo
     */
    public function setNameRepo($name_repo)
    {
        $this->log(sprintf("Target repo is set to '%s'.", $name_repo));
        $this->name_repo = $name_repo;
    }

    /**
     * @return string
     */
    public function getLogFile()
    {
        return $this->log_file;
    }

    /**
     * @param string $log_file
     */
    public function setLogFile($log_file)
    {
        $this->log_file = $log_file;
    }

    /**
     * @return bool
     */
    public function isLinuxOs()
    {
        return $this->is_linux_os;
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     */
    public function setMode($mode)
    {
        $this->log(sprintf("Script mode is set to '%s'.", $mode));
        $this->mode = $mode;
    }

    public static function secureString($string)
    {
        return preg_replace('~[^\w\d\-]~', '-', $string);
    }

    /**
     * @return mixed
     */
    public function getBitbucketUser()
    {
        return $this->bitbucket_user;
    }

    /**
     * @param mixed $bitbucket_user
     */
    public function setBitbucketUser($bitbucket_user)
    {
        $this->bitbucket_user = $bitbucket_user;
    }

    /**
     * @return mixed
     */
    public function getBitbucketPass()
    {
        return $this->bitbucket_pass;
    }

    /**
     * @param mixed $bitbucket_pass
     */
    public function setBitbucketPass($bitbucket_pass)
    {
        $this->bitbucket_pass = $bitbucket_pass;
    }

    /**
     * @return bool
     */
    public function isStopped()
    {
        return $this->stopped;
    }

    /**
     * @param bool $stopped
     */
    public function setStopped($stopped)
    {
        $this->stopped = $stopped;
    }

    /**
     * @return array
     */
    public function getLogs()
    {
        return $this->logs;
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess($success)
    {
        $this->success = $success;
    }
}