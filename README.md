### What is this repository for? ###

Repo is for processing WebHooks provided by BitBucket service.

Basically it's simple Wordpress plugin which accepts POST requests from BitBucket, downloads ZIP archive of tracked branch if needed and extracts it to designated folder.

### Requirements ###

* PHP modules: ZipArchive, cUrl
* Wordpress =)

### How do I get set up? ###

* Install the plugin
* Activate it
* Fill out all the fields in "Settings" page
* Set provided URL as target for your WebHook in BitBucket

### Who do I talk to? ###

* @pecochek at HipChat

### Plans ###

* Visual logs in Wordpress dashboard
* "Forced Update" button to dashboard (to manually get latest branch file from BitBucket)

### Credits ###

* [clipboard.js](https://clipboardjs.com/) by Zeno Rocha
* [toastr](https://github.com/CodeSeven/toastr) by CodeSeven