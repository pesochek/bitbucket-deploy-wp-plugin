<?php

namespace BitbucketDeployWordpressPlugin;

require_once dirname(__FILE__) . '/class.WebHook.php';

class BitbucketDeploy
{
    const PREFIX = 'bbdwp';
    const QUERY_PARAM = 'hook';

    const SETTING_LOGIN = self::PREFIX . '-login';
    const SETTING_PASSWORD = self::PREFIX . '-password';
    const SETTING_REPO = self::PREFIX . '-repo';
    const SETTING_BRANCH = self::PREFIX . '-branch';
    const SETTING_DIR = self::PREFIX . '-dir';
    const SETTING_SECRET = self::PREFIX . '-secret';

    private static $options = array(
        self::SETTING_LOGIN,
        self::SETTING_PASSWORD,
        self::SETTING_REPO,
        self::SETTING_BRANCH,
        self::SETTING_DIR,
        self::SETTING_SECRET,
    );

    public static $assets_url;
    public static $assets_dir;

    private $login;
    private $pass;
    private $repo;
    private $branch;
    private $dir;
    private $secret;

    public function __construct()
    {
        static::$assets_url = plugin_dir_url(__FILE__) . 'assets/';
        static::$assets_dir = plugin_dir_path(__FILE__) . 'assets' . DIRECTORY_SEPARATOR;

        foreach (self::$options as $option) {
            $value = get_option($option);
            if (!$value)
                continue;
            switch ($option) {
                case self::SETTING_LOGIN:
                    $this->login = $value;
                    break;
                case self::SETTING_PASSWORD:
                    $this->pass = $value;
                    break;
                case self::SETTING_REPO:
                    $this->repo = $value;
                    break;
                case self::SETTING_BRANCH:
                    $this->branch = $value;
                    break;
                case self::SETTING_DIR:
                    $this->dir = $value;
                    break;
                case self::SETTING_SECRET:
                    $this->secret = $value;
                    break;
            }
        }
    }

    private static function assets()
    {
        wp_enqueue_style(
            sprintf('%s-main-style', self::PREFIX),
            self::$assets_url . 'css/style.css',
            array(),
            filemtime(self::$assets_dir . 'css/style.css')
        );

        wp_enqueue_style(
            sprintf('%s-toastr', self::PREFIX),
            self::$assets_url . 'css/toastr.min.css'
        );

        wp_enqueue_script(
            sprintf('%s-toastr', self::PREFIX),
            self::$assets_url . 'js/toastr.min.js',
            array('jquery')
        );

        wp_enqueue_script(
            sprintf('%s-copy', self::PREFIX),
            self::$assets_url . 'js/clipboard.min.js'
        );

        wp_enqueue_script(
            sprintf('%s-main-script', self::PREFIX),
            self::$assets_url . 'js/script.js',
            array('jquery'),
            filemtime(self::$assets_dir . 'js/script.js')
        );
    }

    /**
     * @link register_activation_hook()
     */
    public function activation()
    {
        // @todo activation()
    }

    /**
     * @link register_deactivation_hook()
     */
    public function deactivation()
    {
        foreach (self::$options as $option)
            unregister_setting(self::PREFIX, $option);
    }

    /**
     * @link register_uninstall_hook()
     */
    public function uninstall()
    {
        // @todo uninstall()
    }

    /**
     * add_action('admin_menu')
     */
    public function admin_menu()
    {
        $page_title = 'Settings';
        $menu_title = 'Deploy plugin for BitBucket';
        $capability = 'manage_options';
        $menu_slug = sprintf('%s-settings', self::PREFIX);
        $function = array($this, 'settings');
        add_menu_page($page_title, $menu_title, $capability, $menu_slug, $function);

        $sub_menu_title = 'Settings';
        add_submenu_page($menu_slug, $page_title, $sub_menu_title, $capability, $menu_slug, $function);

        add_submenu_page(
            $menu_slug,
            'Manual Update',
            'Manual Update',
            $capability,
            sprintf('%s-manual', self::PREFIX),
            array($this, 'manual'));

        add_submenu_page(
            $menu_slug,
            'Logs',
            'Logs',
            $capability,
            sprintf('%s-logs', self::PREFIX),
            array($this, 'logs'));
    }

    /**
     * add_action('admin_init')
     */
    public function admin_init()
    {
        foreach (self::$options as $option)
            register_setting(self::PREFIX, $option);
    }

    public function plugin_action_links($links, $file)
    {
        static $this_plugin;

        if (!$this_plugin)
            $this_plugin = plugin_basename(__FILE__);

        if ($file == $this_plugin) {
            $url = sprintf('%s/wp-admin/admin.php?page=%s-settings', get_bloginfo('wpurl'), self::PREFIX);
            $settings_link = sprintf('<a href="%s">Settings</a>', $url);
            array_unshift($links, $settings_link);
        }

        return $links;
    }

    /**
     * Settings page
     */
    public function settings()
    {
        if (!current_user_can('manage_options')) {
            wp_die('You do not have sufficient permissions to access this page.');
        }

        self::assets();

        $required_classes_missing = array();
        if (!class_exists('ZipArchive'))
            $required_classes_missing [] = "<p><a href='http://php.net/manual/en/class.ziparchive.php' target='_blank'>ZipArchive class</a> is required for this plugin to work.</p>";
        if (!extension_loaded('curl'))
            $required_classes_missing [] = "<p><a href='http://php.net/curl' target=_blank>cUrl extension</a> is required.</p>";

        if (!empty($required_classes_missing))
            wp_die(sprintf(
                "Several requirements are not met. Plugin cannot be used at the moment.<hr/>%s",
                implode("<br/>", $required_classes_missing)
            ));

        if (isset($_GET['settings-updated'])) {
            // add settings saved message with the class of "updated"
            add_settings_error(
                sprintf('%s_messages', self::PREFIX),
                sprintf('%s_message', self::PREFIX),
                __('Settings Saved', self::PREFIX),
                'updated');
        }

        settings_errors(sprintf('%s_messages', self::PREFIX));
        ?>
        <div class="wrap">
            <form action="options.php" method="post" id="bitbucket-deploy-settings">
                <?php
                settings_fields(self::PREFIX);
                do_settings_sections(self::PREFIX);
                ?>

                <fieldset>
                    <legend>BitBucket access</legend>
                    <div class="row">
                        <p>Login:</p>
                        <input type="text" name="<?= self::SETTING_LOGIN ?>"
                               placeholder="Your account login"
                               value="<?= get_option(self::SETTING_LOGIN) ?>"/>
                    </div>
                    <div class="row">
                        <p>Password:</p>
                        <input type="text" name="<?= self::SETTING_PASSWORD ?>"
                               placeholder="Your account password"
                               value="<?= get_option(self::SETTING_PASSWORD) ?>"/>
                    </div>
                </fieldset>

                <fieldset>
                    <legend>Git settings</legend>
                    <div class="row">
                        <p>Repo:</p>
                        <input type="text" name="<?= self::SETTING_REPO ?>"
                               placeholder="Git repo name"
                               value="<?= get_option(self::SETTING_REPO) ?>"/>
                        <small>{owner}/{repo-name}</small>
                    </div>
                    <div class="row">
                        <p>Branch:</p>
                        <input type="text" name="<?= self::SETTING_BRANCH ?>"
                               placeholder="Target branch to track"
                               value="<?= get_option(self::SETTING_BRANCH) ?>"/>
                    </div>
                </fieldset>

                <fieldset>
                    <legend>Other settings</legend>
                    <div class="row">
                        <p>Target dir:</p>
                        <input type="text" name="<?= self::SETTING_DIR ?>"
                               placeholder="Dir to deploy"
                               value="<?= get_option(self::SETTING_DIR) ?>"/>
                        <small>starting from <strong><?= ABSPATH ?></strong></small>
                    </div>
                    <div class="row">
                        <p>Unique string:</p>
                        <input type="text" id="webhook-secret-value" name="<?= self::SETTING_SECRET ?>"
                               placeholder="Random string"
                               value="<?= get_option(self::SETTING_SECRET) ?>"/>
                        <button id="webhook-secret-generate">random</button>
                        <small>used to create unique WebHook URL (to eliminate possible abuse)</small>
                        <hr/>
                        <p>
                            Resulting URL:
                            <nobr>
                                <span id="webhook-url"></span>
                                <button id="webhook-copy" data-clipboard-target="#webhook-url">copy</button>
                            </nobr>
                            <small>(paste it to BitBucket WebHook section)</small>
                        </p>
                    </div>
                </fieldset>

                <div style="clear: both;"></div>

                <p>
                    <strong>Don't forget to "save settings" to apply new values!</strong>
                    &nbsp; &nbsp;
                    once settings are saved you can proceed with
                    <a href="?page=<?= sprintf('%s-manual', self::PREFIX) ?>" class="button button-secondary">Manual
                        Update</a>
                </p>
                <h1 class="warning">All fields are required for the seamless run of plugin!</h1>

                <?php submit_button('Save Settings'); ?>
            </form>
        </div>
        <?php
    }

    /**
     * Manual update page
     */
    public function manual()
    {
        $logs = array();
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $hook = new WebHook();

            $hook->setWriteLog(false);
            $hook->setDisplayLog(false);

            $hook->setMode(WebHook::MODE_WORDPRESS);
            $hook->setNameRepo($this->repo);
            $hook->setTargetBranch($this->branch);
            $hook->setDirWebRoot(ABSPATH . DIRECTORY_SEPARATOR . $this->dir);
            $hook->setBitbucketUser($this->login);
            $hook->setBitbucketPass($this->pass);

            $hook->update(true);
            $logs = $hook->getLogs();
        }
        ?>

        <div class="wrap">
            <form method="post">
                <textarea readonly style="width: 100%; height: 400px;"
                          placeholder="Update output will be displayed here"><?= implode(PHP_EOL, $logs) ?></textarea>
                <?php submit_button('Run Manual Update'); ?>
                <hr/>
                <small>Will simply download archive of <strong><?= get_option(self::SETTING_BRANCH) ?></strong> branch
                    from <strong><?= get_option(self::SETTING_REPO) ?></strong> repo and extract it to
                    <strong><?= get_option(self::SETTING_DIR) ?></strong></small>
            </form>
        </div>

        <?php
    }

    /**
     * Logs page
     */
    public function logs()
    {
        self::assets();
        // @todo page with logs
        ?>
        <button class="button button-primary">Clear Logs</button>

        <h1>@todo page with logs</h1>
        <?php
    }

    public function parse_request($wp)
    {
        if (
            array_key_exists(self::QUERY_PARAM, $wp->query_vars)
            && $wp->query_vars[self::QUERY_PARAM] == $this->secret
            && $_SERVER['REQUEST_METHOD'] == 'POST'
        ) {
            $hook = new WebHook();

            $hook->setWriteLog(false);
            $hook->setDisplayLog(false);

            $hook->setMode(WebHook::MODE_WORDPRESS);
            $hook->setNameRepo($this->repo);
            $hook->setTargetBranch($this->branch);
            $hook->setDirWebRoot(ABSPATH . DIRECTORY_SEPARATOR . $this->dir);
            $hook->setBitbucketUser($this->login);
            $hook->setBitbucketPass($this->pass);

            $hook->processPayload(file_get_contents('php://input'));
            $hook->checkIfUpdateNeeded();
            $hook->update();

            echo $hook->isSuccess()
                ? "Thank you."
                : "These are not the droids you are looking for.";
            exit;
        }
    }

    public function query_vars($vars)
    {
        if (!in_array(self::QUERY_PARAM, $vars))
            $vars [] = self::QUERY_PARAM;
        return $vars;
    }
}