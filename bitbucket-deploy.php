<?php
/*
 * Plugin Name:     Deploy plugin for BitBucket
 * Plugin URI:      https://bitbucket.org/pesochek/bitbucket-deploy-wp-plugin
 * Description:     Webhook for BitBucket with deploy functionality if needed
 * Version:         1.0.1
 * Author:          Nikolai Pesochenskii
 *
 * https://developer.wordpress.org/plugins/the-basics/activation-deactivation-hooks/
 * https://www.smashingmagazine.com/2011/03/ten-things-every-wordpress-plugin-developer-should-know/
*/

namespace BitbucketDeployWordpressPlugin;

// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
    echo "Hi there!  I'm just a plugin, not much I can do when called directly.";
    exit;
}

require_once dirname(__FILE__) . '/class.BitbucketDeploy.php';

$bbdwp = new BitbucketDeploy();

add_action('parse_request', array($bbdwp, 'parse_request'));

add_filter('query_vars', array($bbdwp, 'query_vars'));

if (is_admin()) {
    register_activation_hook(__FILE__, array($bbdwp, 'activation'));

    register_deactivation_hook(__FILE__, array($bbdwp, 'deactivation'));

    register_uninstall_hook(__FILE__, array($bbdwp, 'uninstall'));

    add_action('admin_menu', array($bbdwp, 'admin_menu'));

    add_action('admin_init', array($bbdwp, 'admin_init'));

    add_filter('plugin_action_links', array($bbdwp, 'plugin_action_links'), 10, 2);
}
