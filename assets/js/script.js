jQuery(function ($) {
    var clipboard = new Clipboard('#webhook-copy');
    clipboard.on('success', function (e) {
        toastr.info('Copied!');
        e.clearSelection();
    });

    updateWebhookUrl($, $('#webhook-secret-value').val());
    $('#bitbucket-deploy-settings')
        .on('click', '#webhook-secret-generate', function (e) {
            e.preventDefault();
            var str = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
            updateWebhookUrl($, str);
        })
        .on('keyup', '#webhook-secret-value', function (e) {
            updateWebhookUrl($, $(this).val());
        })
        .on('click', '#webhook-copy', function (e) {
            e.preventDefault();
        });
});

function updateWebhookUrl($, str) {
    var url = location.origin + '/?hook=' + str;
    $('#webhook-secret-value').val(str);
    $('#webhook-url').text(url);
}